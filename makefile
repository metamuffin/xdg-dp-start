
DEPS = libpipewire-0.3 glib-2.0 gio-2.0
CFLAGS = $(shell pkg-config --cflags $(DEPS))
LDFLAGS = $(shell pkg-config --libs $(DEPS))

BIN = xdg-dp-start

$(BIN): $(BIN).o
	gcc $(LDFLAGS) $^ -o $@
$(BIN).o: $(BIN).c
	gcc $(CFLAGS) -c $< -o $@

compile_flags.txt: makefile
	echo $(CFLAGS) | sed -e 's/ /\n/g' > $@

install: $(BIN)
	install -m755 $(BIN) /usr/local/bin/$(BIN)
