# xdg-dp-start

Utility to start the xdg-desktop-portal from the command line.

## Installation

```
make
make install
```

## Usage

```sh
xdg-dp-start
```
