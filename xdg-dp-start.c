/*
	xdg-dp-start - start xdg-desktop-portal from the command line
	Copyright (C) 2023 metamuffin
 	Copyright 2021 Georges Basile Stavracas Neto <georges.stavracas@gmail.com>

	This program is adapted from OBS Studio's /plugins/linux-pipewire/screencast-portal.c 
	which is licensed as GNU General Public License Version 2

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as
	published by the Free Software Foundation, version 3 of the
	License only.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with this program. If not, see <http://www.gnu.org/licenses/>.

*/

#define _GNU_SOURCE

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <unistd.h>
#include <gio/gunixfdlist.h>
#include <signal.h>

#define UNUSED_PARAMETER(_) 0
#define DIE(...)                      \
	{                                 \
		fprintf(stderr, "error: ");   \
		fprintf(stderr, __VA_ARGS__); \
		fprintf(stderr, "\n");        \
		exit(1);                      \
	}

#define REQUEST_PATH "/org/freedesktop/portal/desktop/request/%s/obs%u"
#define SESSION_PATH "/org/freedesktop/portal/desktop/session/%s/obs%u"

static GDBusConnection* connection = NULL;

enum portal_capture_type {
	PORTAL_CAPTURE_TYPE_MONITOR = 1 << 0,
	PORTAL_CAPTURE_TYPE_WINDOW = 1 << 1,
	PORTAL_CAPTURE_TYPE_VIRTUAL = 1 << 2,
};

enum portal_cursor_mode {
	PORTAL_CURSOR_MODE_HIDDEN = 1 << 0,
	PORTAL_CURSOR_MODE_EMBEDDED = 1 << 1,
	PORTAL_CURSOR_MODE_METADATA = 1 << 2,
};

struct screencast_portal_capture {
	enum portal_capture_type capture_type;
	GCancellable* cancellable;
	char* session_handle;
	uint32_t pipewire_node;
	char cursor_visible;
};

static void ensure_connection(void) {
	g_autoptr(GError) error = NULL;
	if (!connection) {
		connection = g_bus_get_sync(G_BUS_TYPE_SESSION, NULL, &error);

		if (error) {
			DIE("error retrieving D-Bus connection: %s", error->message);
			return;
		}
	}
}

char* get_sender_name(void) {
	char* sender_name;
	char* aux;

	ensure_connection();

	sender_name = strdup(g_dbus_connection_get_unique_name(connection) + 1);

	while ((aux = strstr(sender_name, ".")) != NULL)
		*aux = '_';

	return sender_name;
}

GDBusConnection* portal_get_dbus_connection(void) {
	ensure_connection();
	return connection;
}

void portal_create_request_path(char** out_path, char** out_token) {
	static uint32_t request_token_count = 0;
	request_token_count++;
	if (out_token) {
		asprintf(out_token, "obs%u", request_token_count);
	}
	if (out_path) {
		char* sender_name = get_sender_name();
		asprintf(out_path, REQUEST_PATH, sender_name, request_token_count);
		free(sender_name);
	}
}

void portal_create_session_path(char** out_path, char** out_token) {
	static uint32_t session_token_count = 0;
	session_token_count++;
	if (out_token) {
		asprintf(out_token, "obs%u", session_token_count);
	}
	if (out_path) {
		char* sender_name;
		sender_name = get_sender_name();
		asprintf(out_path, SESSION_PATH, sender_name, session_token_count);
		free(sender_name);
	}
}

static GDBusProxy* screencast_proxy = NULL;

static void ensure_screencast_portal_proxy(void) {
	g_autoptr(GError) error = NULL;
	if (!screencast_proxy) {
		screencast_proxy = g_dbus_proxy_new_sync(
		        portal_get_dbus_connection(), G_DBUS_PROXY_FLAGS_NONE, NULL,
		        "org.freedesktop.portal.Desktop", "/org/freedesktop/portal/desktop",
		        "org.freedesktop.portal.ScreenCast", NULL, &error);

		if (error) {
			DIE("error retrieving D-Bus proxy: %s", error->message);
			return;
		}
	}
}

static GDBusProxy* get_screencast_portal_proxy(void) {
	ensure_screencast_portal_proxy();
	return screencast_proxy;
}

static uint32_t get_available_cursor_modes(void) {
	g_autoptr(GVariant) cached_cursor_modes = NULL;
	uint32_t available_cursor_modes;

	ensure_screencast_portal_proxy();

	if (!screencast_proxy)
		return 0;

	cached_cursor_modes
	        = g_dbus_proxy_get_cached_property(screencast_proxy, "AvailableCursorModes");
	available_cursor_modes
	        = cached_cursor_modes ? g_variant_get_uint32(cached_cursor_modes) : 0;

	return available_cursor_modes;
}

static uint32_t get_screencast_version(void) {
	g_autoptr(GVariant) cached_version = NULL;
	uint32_t version;

	ensure_screencast_portal_proxy();

	if (!screencast_proxy)
		return 0;

	cached_version = g_dbus_proxy_get_cached_property(screencast_proxy, "version");
	version = cached_version ? g_variant_get_uint32(cached_version) : 0;

	return version;
}

struct dbus_call_data {
	struct screencast_portal_capture* capture;
	char* request_path;
	guint signal_id;
	gulong cancelled_id;
};

static const char* capture_type_to_string(enum portal_capture_type capture_type) {
	switch (capture_type) {
	case PORTAL_CAPTURE_TYPE_MONITOR:
		return "desktop";
	case PORTAL_CAPTURE_TYPE_WINDOW:
		return "window";
	case PORTAL_CAPTURE_TYPE_VIRTUAL:
	default:
		return "unknown";
	}
	return "unknown";
}

static void on_cancelled_cb(GCancellable* cancellable, void* data) {
	UNUSED_PARAMETER(cancellable);

	struct dbus_call_data* call = data;

	g_dbus_connection_call(portal_get_dbus_connection(), "org.freedesktop.portal.Desktop",
	                       call->request_path, "org.freedesktop.portal.Request", "Close", NULL,
	                       NULL, G_DBUS_CALL_FLAGS_NONE, -1, NULL, NULL, NULL);

	DIE("cancelled");
}

static struct dbus_call_data* subscribe_to_signal(struct screencast_portal_capture* capture,
                                                  const char* path,
                                                  GDBusSignalCallback callback) {
	struct dbus_call_data* call;

	call = malloc(sizeof(struct dbus_call_data));
	call->capture = capture;
	call->request_path = strdup(path);
	call->cancelled_id = g_signal_connect(capture->cancellable, "cancelled",
	                                      G_CALLBACK(on_cancelled_cb), call);
	call->signal_id = g_dbus_connection_signal_subscribe(
	        portal_get_dbus_connection(), "org.freedesktop.portal.Desktop",
	        "org.freedesktop.portal.Request", "Response", call->request_path, NULL,
	        G_DBUS_SIGNAL_FLAGS_NO_MATCH_RULE, callback, call, NULL);

	return call;
}

static void dbus_call_data_free(struct dbus_call_data* call) {
	if (!call)
		return;

	if (call->signal_id)
		g_dbus_connection_signal_unsubscribe(portal_get_dbus_connection(), call->signal_id);

	if (call->cancelled_id > 0)
		g_signal_handler_disconnect(call->capture->cancellable, call->cancelled_id);

	g_clear_pointer(&call->request_path, free);
	free(call);
}

static void on_pipewire_remote_opened_cb(GObject* source, GAsyncResult* res, void* user_data) {
	struct screencast_portal_capture* capture;
	g_autoptr(GUnixFDList) fd_list = NULL;
	g_autoptr(GVariant) result = NULL;
	g_autoptr(GError) error = NULL;
	int pipewire_fd;
	int fd_index;

	capture = user_data;
	result = g_dbus_proxy_call_with_unix_fd_list_finish(G_DBUS_PROXY(source), &fd_list, res,
	                                                    &error);
	if (error) {
		if (!g_error_matches(error, G_IO_ERROR, G_IO_ERROR_CANCELLED))
			DIE("[pipewire] error retrieving pipewire fd: %s", error->message);
		return;
	}

	g_variant_get(result, "(h)", &fd_index, &error);

	pipewire_fd = g_unix_fd_list_get(fd_list, fd_index, &error);
	if (error) {
		if (!g_error_matches(error, G_IO_ERROR, G_IO_ERROR_CANCELLED))
			DIE("[pipewire] error retrieving pipewire fd: %s", error->message);
		return;
	}

	printf("/proc/%i/fd/%i\n", getpid(), pipewire_fd);
}

static void open_pipewire_remote(struct screencast_portal_capture* capture) {
	GVariantBuilder builder;

	g_variant_builder_init(&builder, G_VARIANT_TYPE_VARDICT);

	g_dbus_proxy_call_with_unix_fd_list(
	        get_screencast_portal_proxy(), "OpenPipeWireRemote",
	        g_variant_new("(oa{sv})", capture->session_handle, &builder),
	        G_DBUS_CALL_FLAGS_NONE, -1, NULL, capture->cancellable,
	        on_pipewire_remote_opened_cb, capture);
}

static void on_start_response_received_cb(GDBusConnection* connection, const char* sender_name,
                                          const char* object_path, const char* interface_name,
                                          const char* signal_name, GVariant* parameters,
                                          void* user_data) {
	UNUSED_PARAMETER(connection);
	UNUSED_PARAMETER(sender_name);
	UNUSED_PARAMETER(object_path);
	UNUSED_PARAMETER(interface_name);
	UNUSED_PARAMETER(signal_name);

	struct screencast_portal_capture* capture;
	g_autoptr(GVariant) stream_properties = NULL;
	g_autoptr(GVariant) streams = NULL;
	g_autoptr(GVariant) result = NULL;
	struct dbus_call_data* call = user_data;
	GVariantIter iter;
	uint32_t response;
	size_t n_streams;

	capture = call->capture;
	g_clear_pointer(&call, dbus_call_data_free);

	g_variant_get(parameters, "(u@a{sv})", &response, &result);

	if (response != 0) {
		DIE("failed to start screencast, denied or cancelled by user");
		return;
	}

	streams = g_variant_lookup_value(result, "streams", G_VARIANT_TYPE_ARRAY);

	g_variant_iter_init(&iter, streams);

	n_streams = g_variant_iter_n_children(&iter);
	if (n_streams != 1)
		DIE("received more than one stream when only one was expected. this is probably a bug in the desktop portal implementation you are using.");

	g_variant_iter_loop(&iter, "(u@a{sv})", &capture->pipewire_node, &stream_properties);
	open_pipewire_remote(capture);
}

static void on_started_cb(GObject* source, GAsyncResult* res, void* user_data) {
	UNUSED_PARAMETER(user_data);

	g_autoptr(GVariant) result = NULL;
	g_autoptr(GError) error = NULL;

	result = g_dbus_proxy_call_finish(G_DBUS_PROXY(source), res, &error);
	if (error) {
		if (!g_error_matches(error, G_IO_ERROR, G_IO_ERROR_CANCELLED))
			DIE("error selecting screencast source: %s", error->message);
		return;
	}
}

static void start(struct screencast_portal_capture* capture) {
	GVariantBuilder builder;
	struct dbus_call_data* call;
	char* request_token;
	char* request_path;

	portal_create_request_path(&request_path, &request_token);

	call = subscribe_to_signal(capture, request_path, on_start_response_received_cb);

	g_variant_builder_init(&builder, G_VARIANT_TYPE_VARDICT);
	g_variant_builder_add(&builder, "{sv}", "handle_token",
	                      g_variant_new_string(request_token));

	g_dbus_proxy_call(get_screencast_portal_proxy(), "Start",
	                  g_variant_new("(osa{sv})", capture->session_handle, "", &builder),
	                  G_DBUS_CALL_FLAGS_NONE, -1, capture->cancellable, on_started_cb, call);

	free(request_token);
	free(request_path);
}

static void on_select_source_response_received_cb(GDBusConnection* connection,
                                                  const char* sender_name,
                                                  const char* object_path,
                                                  const char* interface_name,
                                                  const char* signal_name, GVariant* parameters,
                                                  void* user_data) {
	UNUSED_PARAMETER(connection);
	UNUSED_PARAMETER(sender_name);
	UNUSED_PARAMETER(object_path);
	UNUSED_PARAMETER(interface_name);
	UNUSED_PARAMETER(signal_name);

	struct screencast_portal_capture* capture;
	g_autoptr(GVariant) ret = NULL;
	struct dbus_call_data* call = user_data;
	uint32_t response;

	capture = call->capture;
	g_clear_pointer(&call, dbus_call_data_free);

	g_variant_get(parameters, "(u@a{sv})", &response, &ret);

	if (response != 0) {
		DIE("failed to select source, denied or cancelled by user\n");
		return;
	}

	start(capture);
}

static void on_source_selected_cb(GObject* source, GAsyncResult* res, void* user_data) {
	UNUSED_PARAMETER(user_data);

	g_autoptr(GVariant) result = NULL;
	g_autoptr(GError) error = NULL;

	result = g_dbus_proxy_call_finish(G_DBUS_PROXY(source), res, &error);
	if (error) {
		if (!g_error_matches(error, G_IO_ERROR, G_IO_ERROR_CANCELLED))
			DIE("error selecting screencast source: %s", error->message);
		return;
	}
}

static void select_source(struct screencast_portal_capture* capture) {
	struct dbus_call_data* call;
	GVariantBuilder builder;
	uint32_t available_cursor_modes;
	char* request_token;
	char* request_path;

	portal_create_request_path(&request_path, &request_token);

	call = subscribe_to_signal(capture, request_path, on_select_source_response_received_cb);

	g_variant_builder_init(&builder, G_VARIANT_TYPE_VARDICT);
	g_variant_builder_add(&builder, "{sv}", "types",
	                      g_variant_new_uint32(capture->capture_type));
	g_variant_builder_add(&builder, "{sv}", "multiple", g_variant_new_boolean(FALSE));
	g_variant_builder_add(&builder, "{sv}", "handle_token",
	                      g_variant_new_string(request_token));

	available_cursor_modes = get_available_cursor_modes();

	if (available_cursor_modes & PORTAL_CURSOR_MODE_METADATA)
		g_variant_builder_add(&builder, "{sv}", "cursor_mode",
		                      g_variant_new_uint32(PORTAL_CURSOR_MODE_METADATA));
	else if ((available_cursor_modes & PORTAL_CURSOR_MODE_EMBEDDED) && capture->cursor_visible)
		g_variant_builder_add(&builder, "{sv}", "cursor_mode",
		                      g_variant_new_uint32(PORTAL_CURSOR_MODE_EMBEDDED));
	else
		g_variant_builder_add(&builder, "{sv}", "cursor_mode",
		                      g_variant_new_uint32(PORTAL_CURSOR_MODE_HIDDEN));

	if (get_screencast_version() >= 4) {
		g_variant_builder_add(&builder, "{sv}", "persist_mode", g_variant_new_uint32(2));
	}

	g_dbus_proxy_call(get_screencast_portal_proxy(), "SelectSources",
	                  g_variant_new("(oa{sv})", capture->session_handle, &builder),
	                  G_DBUS_CALL_FLAGS_NONE, -1, capture->cancellable, on_source_selected_cb,
	                  call);

	free(request_token);
	free(request_path);
}

static void on_create_session_response_received_cb(GDBusConnection* connection,
                                                   const char* sender_name,
                                                   const char* object_path,
                                                   const char* interface_name,
                                                   const char* signal_name,
                                                   GVariant* parameters, void* user_data) {
	UNUSED_PARAMETER(connection);
	UNUSED_PARAMETER(sender_name);
	UNUSED_PARAMETER(object_path);
	UNUSED_PARAMETER(interface_name);
	UNUSED_PARAMETER(signal_name);

	struct screencast_portal_capture* capture;
	g_autoptr(GVariant) session_handle_variant = NULL;
	g_autoptr(GVariant) result = NULL;
	struct dbus_call_data* call = user_data;
	uint32_t response;

	capture = call->capture;

	g_clear_pointer(&call, dbus_call_data_free);

	g_variant_get(parameters, "(u@a{sv})", &response, &result);

	if (response != 0) {
		DIE("failed to create session, denied or cancelled by user");
		return;
	}

	session_handle_variant = g_variant_lookup_value(result, "session_handle", NULL);
	capture->session_handle = g_variant_dup_string(session_handle_variant, NULL);

	select_source(capture);
}

static void on_session_created_cb(GObject* source, GAsyncResult* res, void* user_data) {
	UNUSED_PARAMETER(user_data);

	g_autoptr(GVariant) result = NULL;
	g_autoptr(GError) error = NULL;

	result = g_dbus_proxy_call_finish(G_DBUS_PROXY(source), res, &error);
	if (error) {
		if (!g_error_matches(error, G_IO_ERROR, G_IO_ERROR_CANCELLED))
			DIE("error creating screencast session: %s", error->message);
		return;
	}
}

static void create_session(struct screencast_portal_capture* capture) {
	struct dbus_call_data* call;
	GVariantBuilder builder;
	char* session_token;
	char* request_token;
	char* request_path;

	portal_create_request_path(&request_path, &request_token);
	portal_create_session_path(NULL, &session_token);

	call = subscribe_to_signal(capture, request_path, on_create_session_response_received_cb);

	g_variant_builder_init(&builder, G_VARIANT_TYPE_VARDICT);
	g_variant_builder_add(&builder, "{sv}", "handle_token",
	                      g_variant_new_string(request_token));
	g_variant_builder_add(&builder, "{sv}", "session_handle_token",
	                      g_variant_new_string(session_token));

	g_dbus_proxy_call(get_screencast_portal_proxy(), "CreateSession",
	                  g_variant_new("(a{sv})", &builder), G_DBUS_CALL_FLAGS_NONE, -1,
	                  capture->cancellable, on_session_created_cb, call);

	free(session_token);
	free(request_token);
	free(request_path);
}

static gboolean init_screencast_capture(struct screencast_portal_capture* capture) {
	GDBusConnection* connection;
	GDBusProxy* proxy;

	capture->cancellable = g_cancellable_new();
	connection = portal_get_dbus_connection();
	if (!connection)
		return FALSE;
	proxy = get_screencast_portal_proxy();
	if (!proxy)
		return FALSE;

	create_session(capture);

	return TRUE;
}

struct screencast_portal_capture*
screencast_portal_desktop_capture_create(enum portal_capture_type type, bool cursor_visible) {
	struct screencast_portal_capture* capture;

	capture = malloc(sizeof(struct screencast_portal_capture));
	capture->capture_type = type;
	capture->cursor_visible = cursor_visible;

	init_screencast_capture(capture);

	return capture;
}

void screencast_portal_capture_destroy(struct screencast_portal_capture* capture) {
	if (!capture)
		return;

	if (capture->session_handle) {
		g_dbus_connection_call(portal_get_dbus_connection(), "org.freedesktop.portal.Desktop",
		                       capture->session_handle, "org.freedesktop.portal.Session",
		                       "Close", NULL, NULL, G_DBUS_CALL_FLAGS_NONE, -1, NULL, NULL,
		                       NULL);

		g_clear_pointer(&capture->session_handle, g_free);
	}

	g_cancellable_cancel(capture->cancellable);
	g_clear_object(&capture->cancellable);
	free(capture);
}

struct screencast_portal_capture* capture;
void sigint_handler(int x) {
	screencast_portal_capture_destroy(capture);
	capture = NULL;
	fprintf(stderr, "exited properly\n");
	exit(0);
}

int main(int argc, char** argv) {
	capture = screencast_portal_desktop_capture_create(PORTAL_CAPTURE_TYPE_MONITOR, true);
	signal(SIGINT, sigint_handler);

	while (1)
		g_main_context_iteration(NULL, 1);

	return 0;
}
